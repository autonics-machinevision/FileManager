﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace FileManager
{
    /// <summary>
    /// DashBoardView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class DashBoardView : UserControl
    {
        List<int> _listTimeDirection = new List<int>();
        List<int> _listActionDirection = new List<int>();
        List<int> _listLogDirection = new List<int>();
        List<string> _listClickColumn = new List<string>();

        BackgroundWorker _backWorkerImage = new BackgroundWorker();
        BackgroundWorker _backWorkerRecipe = new BackgroundWorker();
        BackgroundWorker _backWorkerLog = new BackgroundWorker();
        BackgroundWorker _backWorkerResult = new BackgroundWorker();

        DashBoardViewModel model;

        public DashBoardView()
        {
            InitializeComponent();

            for(int i = 0; i < 4; i++)
            {
                _listTimeDirection.Add(1);
                _listActionDirection.Add(1);
                _listLogDirection.Add(1);
                _listClickColumn.Add("");
            }

            _backWorkerImage.DoWork += BackgroundWorker_DoWork;
            _backWorkerRecipe.DoWork += BackgroundWorker_DoWork;
            _backWorkerLog.DoWork += BackgroundWorker_DoWork;
            _backWorkerResult.DoWork += BackgroundWorker_DoWork;

            _backWorkerImage.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
            _backWorkerRecipe.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
            _backWorkerLog.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
            _backWorkerResult.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;

            ImageProgress.Visibility = Visibility.Hidden;
            RecipeProgress.Visibility = Visibility.Hidden;
            LogProgress.Visibility = Visibility.Hidden;
            ResultProgress.Visibility = Visibility.Hidden;
        }

        void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int index = 0;

            if (_backWorkerImage == sender)
                index = 0;
            else if (_backWorkerRecipe == sender)
                index = 1;
            else if (_backWorkerLog == sender)
                index = 2;
            else if (_backWorkerResult == sender)
                index = 3;

            ObservableCollection<Data> data = null;
            ListView listView = null;

            switch (index)
            {
                case 0:
                    data = model.ImageItems;
                    listView = listImageLog;
                    break;
                case 1:
                    data = model.RecipeItems;
                    listView = listRecipeLog;
                    break;
                case 2:
                    data = model.LogItems;
                    listView = listLogLog;
                    break;
                case 3:
                    data = model.ResultItems;
                    listView = listResultLog;
                    break;
            }

            listView.Dispatcher.Invoke(() =>
            {
                listView.ItemsSource = null;
            });


            if (_listClickColumn[index] == "Time")
            {
                _listTimeDirection[index] *= -1;

                if (_listTimeDirection[index] == -1)
                    data = new ObservableCollection<Data>(data.OrderByDescending(i => i.Time));
                else
                    data = new ObservableCollection<Data>(data.OrderBy(i => i.Time));
            }
            else if (_listClickColumn[index] == "Action")
            {
                _listActionDirection[index] *= -1;

                if(_listActionDirection[index] == -1)
                    data = new ObservableCollection<Data>(data.OrderByDescending(i => i.Action));
                else
                    data = new ObservableCollection<Data>(data.OrderBy(i => i.Action));
            }
            else if (_listClickColumn[index] == "Log")
            {
                _listLogDirection[index] *= -1;

                if (_listLogDirection[index] == -1)
                    data = new ObservableCollection<Data>(data.OrderByDescending(i => i.Msg));
                else
                    data = new ObservableCollection<Data>(data.OrderBy(i => i.Msg));
            }
            switch (index)
            {
                case 0:
                    model.ImageItems = data;
                    break;
                case 1:
                    model.RecipeItems = data;
                    break;
                case 2:
                    model.LogItems = data;
                    break;
                case 3:
                    model.ResultItems = data;
                    break;
            }
        }

        void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(!e.Cancelled && e.Error == null)
            {
                if (_backWorkerImage == sender)
                {
                    ImageProgress.Visibility = Visibility.Hidden;
                    listImageLog.ItemsSource = model.ImageItems;
                }
                else if (_backWorkerRecipe == sender)
                {
                    RecipeProgress.Visibility = Visibility.Hidden;
                    listRecipeLog.ItemsSource = model.RecipeItems;
                }
                else if (_backWorkerLog == sender)
                {
                    LogProgress.Visibility = Visibility.Hidden;
                    listLogLog.ItemsSource = model.LogItems;
                }
                else if (_backWorkerResult == sender)
                {
                    ResultProgress.Visibility = Visibility.Hidden;
                    listResultLog.ItemsSource = model.ResultItems;
                }
            }
        }
        private void ListLog_Click(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource.GetType() == typeof(GridViewColumnHeader))
            {
                int index = 0;
                BackgroundWorker worker = null;
                var header = (GridViewColumnHeader)e.OriginalSource;
                model = (DashBoardViewModel)DataContext;

                if (sender == listImageLog)
                {
                    index = 0;
                    worker = _backWorkerImage;
                    ImageProgress.Visibility = Visibility.Visible;
                }
                else if(sender == listRecipeLog)
                {
                    index = 1;
                    worker = _backWorkerRecipe;
                    RecipeProgress.Visibility = Visibility.Visible;
                }
                else if (sender == listLogLog)
                {
                    index = 2;
                    worker = _backWorkerLog;
                    LogProgress.Visibility = Visibility.Visible;
                }
                else if (sender == listResultLog)
                {
                    index = 3;
                    worker = _backWorkerResult;
                    ResultProgress.Visibility = Visibility.Visible;
                }

                _listClickColumn[index] = header.Content.ToString();

                worker.RunWorkerAsync(5000);
            }
        }

        private void BtnFolder_Click(object sender, RoutedEventArgs e)
        {
            if(sender == BtnImageFolder)
                System.Diagnostics.Process.Start(@"D:\autonics\IMTInspector\image");
            else if (sender == BtnRecipeFolder)
                System.Diagnostics.Process.Start(@"D:\autonics\IMTInspector\image");
            else if (sender == BtnLogFolder)
                System.Diagnostics.Process.Start(@"D:\autonics\IMTInspector\image");
            else if (sender == BtnResultFolder)
                System.Diagnostics.Process.Start(@"D:\autonics\IMTInspector\image");
        }

        private void ListViewItem_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            var item = sender as ListViewItem;
            
            if (item != null && item.IsSelected)
            {
                //Do your stuff
            }
        }

        private void ListViewItem_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            
            
        }
    }
}
