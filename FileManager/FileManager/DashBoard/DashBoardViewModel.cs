﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Media;

namespace FileManager
{
    public enum Action { Add, Del }

    class DashBoardViewModel : ObservableObject, IPageViewModel
    {
        private string _imageMaxVolume = "1024";
        private string _recipeMaxVolume = "1024";
        private string _logMaxVolume = "1024";
        private string _resultMaxVolume = "1024";

        private int _imageCurrentVolume = 999;
        private int _recipeCurrentVolume = 256;
        private int _logCurrentVolume = 512;
        private int _resultCurrentVolume = 812;

        private Brush _imageBrush = Brushes.Red;
        private Brush _recipeBrush = Brushes.Red;
        private Brush _logBrush = Brushes.Red;
        private Brush _resultBrush = Brushes.Red;

        private ObservableCollection <Data> _listImageLog = new ObservableCollection<Data>();
        private ObservableCollection <Data> _listRecipeLog = new ObservableCollection<Data>();
        private ObservableCollection <Data> _listLogLog = new ObservableCollection<Data>();
        private ObservableCollection<Data> _listResultLog = new ObservableCollection<Data>();

        private PackIconKind _imageIcon;
        private PackIconKind _recipeIcon;
        private PackIconKind _logIcon;
        private PackIconKind _resultIcon;

        private int _index = 0;
        private Timer _timer = new Timer();
        public string Name
        {
            get { return "DashBoard"; }
        }

        public Brush ImageColor
        {
            get { return _imageBrush; }
            set { _imageBrush = value; }
        }

        public Brush RecipeColor
        {
            get { return _recipeBrush; }
            set { _recipeBrush = value; }
        }

        public Brush LogColor
        {
            get { return _logBrush; }
            set { _logBrush = value; }
        }

        public Brush ResultColor
        {
            get { return _resultBrush; }
            set { _resultBrush = value; }
        }

        public string ImageMaxVolume
        {
            get { return _imageMaxVolume; }
            set { _imageMaxVolume = value; }
        }

        public string RecipeMaxVolume
        {
            get { return _recipeMaxVolume; }
            set { _recipeMaxVolume = value; }
        }

        public string LogMaxVolume
        {
            get { return _logMaxVolume; }
            set { _logMaxVolume = value; }
        }

        public string ResultMaxVolume
        {
            get { return _resultMaxVolume; }
            set { _resultMaxVolume = value; }
        }

        public int ImageCurrentVolume
        {
            get { return _imageCurrentVolume; }
            set { _imageCurrentVolume = value; }
        }

        public int RecipeCurrentVolume
        {
            get { return _recipeCurrentVolume; }
            set { _recipeCurrentVolume = value; }
        }

        public int LogCurrentVolume
        {
            get { return _logCurrentVolume; }
            set { _logCurrentVolume = value; }
        }

        public int ResultCurrentVolume
        {
            get { return _resultCurrentVolume; }
            set { _resultCurrentVolume = value; }
        }

        public int ImagePercent
        {
            get { return (int)((float)Convert.ToInt16(_imageCurrentVolume) / Convert.ToInt16(_imageMaxVolume) * 100); }
            set { }
        }

        public int RecipePercent
        {
            get { return (int)((float)Convert.ToInt16(_recipeCurrentVolume) / Convert.ToInt16(_recipeMaxVolume) * 100); }
            set { }
        }

        public int LogPercent
        {
            get { return (int)((float)Convert.ToInt16(_logCurrentVolume) / Convert.ToInt16(_logMaxVolume) * 100); }
            set { }
        }
        public int ResultPercent
        {
            get { return (int)((float)Convert.ToInt16(_resultCurrentVolume) / Convert.ToInt16(_resultMaxVolume) * 100); }
            set { }
        }
        public string ImagePercentString
        {
            get { return string.Format("{0:f0}", ((float)Convert.ToInt16(_imageCurrentVolume) / Convert.ToInt16(_imageMaxVolume) * 100)) + "%"; }
        }

        public string RecipePercentString
        {
            get { return string.Format("{0:f0}", ((float)Convert.ToInt16(_recipeCurrentVolume) / Convert.ToInt16(_recipeMaxVolume) * 100)) + "%"; }
        }

        public string LogPercentString
        {
            get { return string.Format("{0:f0}", ((float)Convert.ToInt16(_logCurrentVolume) / Convert.ToInt16(_logMaxVolume) * 100)) + "%"; }
        }

        public string ResultPercentString
        {
            get { return string.Format("{0:f0}", ((float)Convert.ToInt16(_resultCurrentVolume) / Convert.ToInt16(_resultMaxVolume) * 100))  + "%"; }
        }

        public string ImageVolume
        {
            get { return _imageCurrentVolume.ToString() + "/" + _imageMaxVolume; }
        }

        public string RecipeVolume
        {
            get { return _recipeCurrentVolume.ToString() + "/" + _recipeMaxVolume; }
        }

        public string LogVolume
        {
            get { return _logCurrentVolume.ToString() + "/" + _logMaxVolume; }
        }

        public string ResultVolume
        {
            get { return _resultCurrentVolume.ToString() + "/" + _resultMaxVolume; }
        }

        public string ImageInfo
        {
            get { if (((float)Convert.ToInt16(_imageCurrentVolume) / Convert.ToInt16(_imageMaxVolume) * 100) > 90) { ImageIcon = PackIconKind.AlertOutline; return "용량이 가득 찼습니다."; } else { return ""; } }
        }

        public string RecipeInfo
        {
            get { if (((float)Convert.ToInt16(_recipeCurrentVolume) / Convert.ToInt16(_recipeMaxVolume) * 100) > 90) { return "용량이 가득 찼습니다."; } else { return ""; } }
        }

        public string LogInfo
        {
            get { if (((float)Convert.ToInt16(_logCurrentVolume) / Convert.ToInt16(_logMaxVolume) * 100) > 90) { return "용량이 가득 찼습니다."; } else { return ""; } }
        }

        public string ResultInfo
        {
            get { if (((float)Convert.ToInt16(_resultCurrentVolume) / Convert.ToInt16(_resultMaxVolume) * 100) > 90) { return "용량이 가득 찼습니다."; } else { return ""; } }
        }

        public PackIconKind ImageIcon
        {
            get { return _imageIcon; }
            set { _imageIcon = value; }
        }

        public PackIconKind RecipeIcon
        {
            get { return _imageIcon; }
            set { _imageIcon = value; }
        }
        
        public ObservableCollection<Data> ImageItems
        {
            get { return _listImageLog; }
            set { _listImageLog = value; }
        }

        public ObservableCollection<Data> RecipeItems
        {
            get { return _listRecipeLog; }
            set { _listRecipeLog = value; }
        }

        public ObservableCollection<Data> LogItems
        {
            get { return _listLogLog; }
            set { _listLogLog = value; }
        }

        public ObservableCollection<Data> ResultItems
        {
            get { return _listResultLog; }
            set { _listResultLog = value; }
        }

        public DashBoardViewModel()
        {
            _timer.Interval = 10;
            _timer.Tick += _timer_Tick;

            Data item = new Data();

            for (int i = 0; i < 100000; i++)
            {
                item = new Data();
                item.Action = (FileManager.Action)Enum.Parse(typeof(FileManager.Action), (i % 2).ToString());
                item.Time = DateTime.Now;
                item.Msg = i.ToString() + ".png";
                item.Background = Brushes.Red;
                _listImageLog.Add(item);

                item = new Data();
                item.Action = (FileManager.Action)Enum.Parse(typeof(FileManager.Action), (i % 2).ToString());
                item.Time = DateTime.Now;
                item.Msg = i.ToString() + ".rcp";
                _listRecipeLog.Add(item);

                item = new Data();
                item.Action = (FileManager.Action)Enum.Parse(typeof(FileManager.Action), (i % 2).ToString());
                item.Time = DateTime.Now;
                item.Msg = i.ToString() + ".log";
                _listLogLog.Add(item);

                item = new Data();
                item.Action = (FileManager.Action)Enum.Parse(typeof(FileManager.Action), (i % 2).ToString());
                item.Time = DateTime.Now;
                item.Msg = i.ToString() + ".result";
                _listResultLog.Add(item);
            }
        }

        private void _timer_Tick(object sender, EventArgs e)
        {

            Data item = new Data();

            item = new Data();
            item.Action = (FileManager.Action)Enum.Parse(typeof(FileManager.Action), (_index % 2).ToString());
            item.Time = DateTime.Now;
            item.Msg = "이미지 추가";
            item.Background = Brushes.Red;
            _listImageLog.Add(item);

            item = new Data();
            item.Action = (FileManager.Action)Enum.Parse(typeof(FileManager.Action), (_index % 2).ToString());
            item.Time = DateTime.Now;
            item.Msg = "레시피 추가";
            _listRecipeLog.Add(item);

            item = new Data();
            item.Action = (FileManager.Action)Enum.Parse(typeof(FileManager.Action), (_index % 2).ToString());
            item.Time = DateTime.Now;
            item.Msg = "로그 추가";
            _listLogLog.Add(item);

            item = new Data();
            item.Action = (FileManager.Action)Enum.Parse(typeof(FileManager.Action), (_index % 2).ToString());
            item.Time = DateTime.Now;
            item.Msg = "결과파일 삭제";
            _listResultLog.Add(item);

            _index++;
        }
    }

    public class Data
    {
        public DateTime Time { get; set; }
        public Action Action { get; set; }
        public string Msg{ get; set; }
        public SolidColorBrush Background { get; set; }
    }
}
