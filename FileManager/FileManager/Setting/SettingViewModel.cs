﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager
{
    public enum ManagementMode { Period, Capacity }

    class SettingViewModel : ObservableObject, IPageViewModel
    {
        List<ManagementData> _listManagementData = new List<ManagementData>();
        public string Name { get { return "SettingView"; } }
        public string ImageFolderPath { get; set; }
        public string RecipeFolderPath { get; set; }
        public string LogFolderPath { get; set; }
        public string ResultFolderPath { get; set; }
        public int Interval { get; set; }
    }

    public class ManagementData
    {
        public ManagementMode Mode { get; set; }
        public string Path { get; set; }
        public string Value { get; set; }
    }
}
