﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using FileManager.Account;

namespace FileManager.Controller
{
    public class AccountManager
    {
        public static List<AccountModel> _listAccount = new List<AccountModel>();
        public static AccountModel _currentAccount = new AccountModel();

        public static bool IsLogin { get; set; }
        public static string ID { get; set; }
        public static string PW { get; set; }

        public AccountManager()
        {
            ID = "autonics";
            PW = GetCrypt("vision1234");
            _currentAccount.ID = ID;
            _currentAccount.PW = PW;
            RegistryManager.SetRegistry("id", ID);
            RegistryManager.SetRegistry("pw", PW);
        }

        public static bool Login(string id, string pw)
        {
            pw = GetCrypt(pw);
            if (string.Compare(id, _currentAccount.ID, false) == 0 && string.Compare(pw, _currentAccount.PW, false) == 0)
            {
                IsLogin = true;
                return IsLogin;
            }

            IsLogin = false;
            return IsLogin;
        }

        public static string GetCrypt(string text)
        {
            try
            {
                var bytes = System.Text.Encoding.UTF8.GetBytes(text);
                using (SHA512 shaM = new SHA512Managed())
                {
                    var hashedInputBytes = shaM.ComputeHash(bytes);
                    // Convert to text
                    // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
                    var hashedInputStringBuilder = new System.Text.StringBuilder(128);
                    foreach (var b in hashedInputBytes)
                        hashedInputStringBuilder.Append(b.ToString("X2"));
                    return hashedInputStringBuilder.ToString();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return "";
        }
    }
}
