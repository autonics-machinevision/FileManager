﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace FileManager.Controller
{
    public static class RegistryManager
    {
        static RegistryKey reg = Registry.CurrentUser.CreateSubKey("SOFTWARE").CreateSubKey("FileManager", true);

        public static void SetRegistry(string name, object value)
        {
            reg.SetValue(name, value);
        }

        public static object GetRegistry(string name)
        {
            return reg.GetValue(name);
        }
    }
}
