﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FileManager.Controller;

namespace FileManager.Account
{
    /// <summary>
    /// LoginForm.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class AccountView : Window
    {
        public AccountView()
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleEsc);
            InitializeControl();
        }

        private void InitializeControl()
        {
            try
            {
                object result = RegistryManager.GetRegistry("remember");

                if (Convert.ToBoolean(result) == true) 
                {
                    TxID.Text = RegistryManager.GetRegistry("id").ToString();
                    ChkRememberID.IsChecked = true;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void HandleEsc(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape) Close();
            //else if (e.Key == Key.Enter) Login();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void Login()
        {
            if (string.IsNullOrEmpty(TxID.Text) || string.IsNullOrEmpty(TxPW.Password))
            {
                MessageBox.Show("아이디 또는 비밀번호를 입력해주세요.");
                return;
            }
            if (!AccountManager.Login(TxID.Text, TxPW.Password))
                return;

            if(ChkRememberID.IsChecked == true)
            {
               
                RegistryManager.SetRegistry("remember", true);
            }

            this.DialogResult = true;
        }
    }
}
