﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FileManager.Controller;
using MaterialDesignThemes.Wpf;
using System.Windows.Forms;
using System.IO;
using FileManager.Account;

namespace FileManager
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        NotifyIcon notifyIcon = new NotifyIcon();
        AccountManager accountManager = new AccountManager();
        public MainWindow()
        {
            InitializeComponent();
            Loaded += Window_Loaded;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.ContextMenu menu = new System.Windows.Forms.ContextMenu();

                System.Windows.Forms.MenuItem item1 = new System.Windows.Forms.MenuItem();
                System.Windows.Forms.MenuItem item2 = new System.Windows.Forms.MenuItem();
                menu.MenuItems.Add(item1);
                menu.MenuItems.Add(item2);
                item1.Index = 0;
                item1.Text = "프로그램 종료";
                item1.Click += delegate (object click, EventArgs eClick)
                {
                    System.Windows.Application.Current.Shutdown();
                };
                item2.Index = 0;
                item2.Text = "프로그램 열기";
                item2.Click += delegate (object click, EventArgs eClick)
                {
                    this.Show();
                    this.WindowState = WindowState.Normal;
                };

                //Stream iconStream = System.Windows.Application.GetResourceStream(new Uri());
                //var iconHandle =
                notifyIcon.Icon = FileManager.Properties.Resources.icon;
                notifyIcon.Visible = true;
                notifyIcon.DoubleClick += delegate (object senders, EventArgs args)
                {
                    this.Show();
                    this.WindowState = WindowState.Normal;
                };
                notifyIcon.ContextMenu = menu;
                notifyIcon.Text = "FileManager";
                BtnDashBoard.IsEnabled = true;
            }
            catch (Exception ex)
            {

            }
        }

        private void BtnMinimize_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.MainWindow.WindowState = WindowState.Minimized;
        }

        private void BtnMaximize_Click(object sender, RoutedEventArgs e)
        {
            if(System.Windows.Application.Current.MainWindow.WindowState != WindowState.Maximized)
                System.Windows.Application.Current.MainWindow.WindowState = WindowState.Maximized;
            else
                System.Windows.Application.Current.MainWindow.WindowState = WindowState.Normal;
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
                BtnMaximize_Click(sender, null);
        }

        protected override void OnStateChanged(EventArgs e)
        {
            if (WindowState.Minimized.Equals(WindowState))
            {
                this.Hide();
            }

            base.OnStateChanged(e);
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            notifyIcon.Dispose();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {

            if(!AccountManager.IsLogin)
            {
                AccountView login = new AccountView();
                if (login.ShowDialog() == true)
                {
                    BtnLogin.Content = new PackIcon { Kind = PackIconKind.LogoutVariant };
                    BtnSetting.IsEnabled = true;
                }
            }
            else
            {
                if(System.Windows.MessageBox.Show("로그아웃을 하시겠습니까?", "로그아웃", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    BtnLogin.Content = new PackIcon { Kind = PackIconKind.Account };
                    AccountManager.IsLogin = false;
                    BtnSetting.IsEnabled = false;
                }
            }

        }

        private void BtnDashBoard_Click(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            {
                MainWindowViewModel mwvm = ((MainWindowViewModel)this.DataContext);
                List<IPageViewModel> listPage = mwvm.PageViewModels;
                if(mwvm.CurrentPageViewModel == listPage[1])
                {
                    mwvm.CurrentPageViewModel = listPage[0];
                    BtnDashBoard.Background = new SolidColorBrush(Color.FromArgb(255, 236, 102, 6));
                    BtnDashBoard.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 236, 102, 6));

                    BtnSetting.Background = new SolidColorBrush(Color.FromArgb(255, 243, 66, 14));
                    BtnSetting.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 243, 66, 14));
                }
            }
        }

        private void BtnSetting_Click(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            {
                MainWindowViewModel mwvm = ((MainWindowViewModel)this.DataContext);
                List<IPageViewModel> listPage = mwvm.PageViewModels;
                if (mwvm.CurrentPageViewModel == listPage[0])
                {
                    mwvm.CurrentPageViewModel = listPage[1];
                    BtnDashBoard.Background = new SolidColorBrush(Color.FromArgb(255, 243, 66, 14));
                    BtnDashBoard.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 243, 66, 14));

                    BtnSetting.Background = new SolidColorBrush(Color.FromArgb(255, 236, 102, 6));
                    BtnSetting.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 236, 102, 6));
                }
            }
        }
    }
}
